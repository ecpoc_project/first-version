package com.example.ivyliang.ec_poc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ivyliang.ec_poc.data.DBhandler;

import java.util.ArrayList;
import java.util.List;

public class button_B2 extends Activity {


    private button_B2 pthis;
    private String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__b2);  //櫃位內容

        pthis = this;

        Intent intent = this.getIntent();
        //取得傳遞過來的資料
        name = intent.getStringExtra("CabinetNum"); //A01
        TextView CabinetNum = findViewById(R.id.CabinetNum);
        CabinetNum.setText(name + "櫃位內容");   //A01櫃位內容
        initListAdapter();

    }

    private void initListAdapter() {
        ListView listView = findViewById(R.id.cabinet_item);

        ListAdapter listAdapter = new ListAdapter(pthis, R.layout.listview_b2, getListRecord());
        listView.setAdapter(listAdapter);


    }

    private List<ListView_b2> getListRecord() {
        //搜尋

        DBhandler handler = new DBhandler(this, null, null, 1);
        return handler.getAcceptCabinetData(name);
    }

    class ListAdapter extends ArrayAdapter<ListView_b2> {

        private Context context;
        private int resource;
        private LinearLayout item_layout;

        public ListAdapter(@NonNull Context context, int resource, @NonNull List<ListView_b2> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                item_layout = new LinearLayout(context);
                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                li.inflate(resource, item_layout, true);
            } else {
                item_layout = (LinearLayout) convertView;
            }
            addItems(getItem(position));

            return item_layout;
        }

        private void addItems(ListView_b2 item) {
            ((TextView)item_layout.findViewById(R.id.Name)).setText(item.getName());
            ((TextView)item_layout.findViewById(R.id.Price)).setText(item.getPrice());
            ((TextView)item_layout.findViewById(R.id.AppearanceSize)).setText(item.getSize());
            ((TextView)item_layout.findViewById(R.id.AppearanceType)).setText(item.getType());
            ((TextView)item_layout.findViewById(R.id.packageNum)).setText(item.getPackageNum());
            ((TextView)item_layout.findViewById(R.id.PhoneNum)).setText(item.getPhoneNum());
        }

    }
}
