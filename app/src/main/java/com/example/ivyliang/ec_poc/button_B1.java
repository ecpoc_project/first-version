package com.example.ivyliang.ec_poc;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ivyliang.ec_poc.data.DBhandler;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

public class button_B1 extends AppCompatActivity {

    private Button btMissData;
    private View camera_btn;
    private button_B1 pthis;
    public TextView txt_response;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__b1);  //今日進貨資料 頁面
        pthis = this;
        Intent intent = this.getIntent();
        initListAdapter();
        camera_btn =(View) findViewById(R.id.camera_btn);
        //txt_response=(TextView) findViewById(R.id.txt_response);
        btMissData = (Button) findViewById(R.id.MissData);

        btMissData.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(button_B1.this,button__a2_1.class);  //從今日差異資料按鈕 到 今日差異資料

                startActivity(intent);//開啟Activity
            }
        });



        //取得當前日期
        Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance().format(calendar.getTime());

        TextView tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setText(currentDate);

        //Button btItem1 = (Button)findViewById(R.id.btItem1); //取得按鈕物件
//
//
//        btItem1.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
//        {
//            public void onClick(View arg0)
//            {
//                Intent intent = new Intent();   //初始化 Intent   物件
//                intent.setClass(button_B1.this,MainActivity.class);  //
//
//                startActivity(intent);//開啟Activity
//            }
//        });

    }
    private void initListAdapter() {

        ListView listView = (ListView) findViewById(R.id.lv_TodayData);

        button_B1.ListAdapter listAdapter = new button_B1.ListAdapter(pthis, R.layout.listview_b1, getListB1Record());
        listView.setAdapter(listAdapter);
    }
    private List<ListView_b1>  getListB1Record() {
        //搜尋

        DBhandler handler = new DBhandler(this, null, null, 1);
        return handler.getTodayInputData();
    }

    class ListAdapter extends ArrayAdapter<ListView_b1> {

        private Context context;
        private int resource;
        private LinearLayout item_layout;

        public ListAdapter(@NonNull Context context, int resource, @NonNull List<ListView_b1> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                item_layout = new LinearLayout(context);
                LayoutInflater li1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                li1.inflate(resource, item_layout, true);
            } else {
                item_layout = (LinearLayout) convertView;
            }
            addItems(getItem(position));

            return item_layout;
        }

        private void addItems(ListView_b1 item) {
            ((TextView)item_layout.findViewById(R.id.CompanyName)).setText(item.getCompanyName());
            ((TextView)item_layout.findViewById(R.id.ArrivalDate)).setText(item.getArrivalDate());
            ((TextView)item_layout.findViewById(R.id.ShouldGoItemA)).setText(item.getShouldGoItemA());
            ((TextView)item_layout.findViewById(R.id.ShouldGoItemB)).setText(item.getShouldGoItemB());
        }

    }


    //掃描條碼
    @Override
    protected void onResume() {
        super.onResume();
        camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pthis,CameraActivity.class);

                startActivityForResult(intent,0);

                //startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle bundle = data.getExtras();
        //txt_response.setText(bundle.getString("scanResult"));
        System.out.print(bundle.getString("scanResult"));

        Intent intent = new Intent();   //初始化 Intent   物件
        intent.setClass(button_B1.this,button_B1_1.class);
        intent .putExtra("PackageNum",bundle.getString("scanResult"));
        startActivity(intent);//開啟Activity
        finish();

    }

}