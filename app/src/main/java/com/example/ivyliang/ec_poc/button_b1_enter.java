package com.example.ivyliang.ec_poc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class button_b1_enter extends AppCompatActivity {

    private View camera_btn;
    private button_b1_enter pthis;
    //public TextView txt_response;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_b1_enter);  //所有櫃位
        pthis = this;
        camera_btn =(View) findViewById(R.id.camera_btn);
        //txt_response=(TextView) findViewById(R.id.txt_response);

        final Button A01 = (Button) findViewById(R.id.A01);
        final Button A02 = (Button) findViewById(R.id.A02);
        final Button A03 = (Button) findViewById(R.id.A03);
        final Button A04 = (Button) findViewById(R.id.A04);
        final Button A05 = (Button) findViewById(R.id.A05);
        final Button A06 = (Button) findViewById(R.id.A06);
        Button Return = (Button) findViewById(R.id.Return);


        A01.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A01.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        A02.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A02.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        A03.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A03.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        A04.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A04.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        A05.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A05.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        A06.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("CabinetNum", A06.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);      // 放入bundle
                intent.setClass(button_b1_enter.this,button_B2.class); // 跳到哪一頁
                startActivity(intent);
            }
        });

        Return.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(button_b1_enter.this,button_D1.class);  //從主畫面 到 退貨明細按鈕

                startActivity(intent);//開啟Activity
            }
        });
    }




    //掃描條碼
    @Override
    protected void onResume() {
        super.onResume();
        camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pthis,CameraActivity.class);

                startActivityForResult(intent,0);

                //startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle bundle = data.getExtras();
        //txt_response.setText(bundle.getString("scanResult"));
        System.out.print(bundle.getString("Main_result"+"scanResult"));

        Intent intent = new Intent();   //初始化 Intent   物件
        intent.setClass(button_b1_enter.this,button_B1_1.class);  //從主畫面 到 驗貨上架按鈕
        intent .putExtra("PackageNum",bundle.getString("scanResult"));
        startActivity(intent);//開啟Activity
        finish();
    }
}
