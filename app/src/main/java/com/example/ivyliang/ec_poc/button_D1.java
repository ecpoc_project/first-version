package com.example.ivyliang.ec_poc;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import static com.example.ivyliang.ec_poc.data.DBhandler.COL_WaitReturnNumA;
import static com.example.ivyliang.ec_poc.data.DBhandler.TABLE_NAME;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_CompanyName;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_ReturnDate;

public class button_D1 extends AppCompatActivity {
    //Toolbar toolbar;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__d1);  //待退貨區  頁面


        //宣告按鈕
        Button WaitReturnNumA = (Button)findViewById(R.id.WaitReturnNumA); //取得按鈕物件
        Button WaitReturnNumB = (Button)findViewById(R.id.WaitReturnNumB); //取得按鈕物件

        WaitReturnNumA.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(button_D1.this,d1_A.class);  //從待退貨區 到  待退貨區_物流A明細
                startActivity(intent);//開啟Activity
            }
        });
//顯示待退貨區物流A的件數
        //開啟資料庫
        db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);
        Cursor a = db.rawQuery("SELECT " + " COUNT  ( " + COL_CompanyName + " ) " + " FROM " + TABLE_NAME +" WHERE " + COL_CompanyName +" = " + " '物流A' " + " and " + COL_ReturnDate + "=" + " date('now','localtime') ", null);
        a.moveToFirst();

//顯示待退貨區物流B的件數
        //開啟資料庫
        db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);
        Cursor b = db.rawQuery("SELECT " + " COUNT  ( " + COL_CompanyName + " ) " + " FROM " + TABLE_NAME +" WHERE " + COL_CompanyName +" = " + " '物流B' " + " and " + COL_ReturnDate + "=" + " date('now','localtime') ", null);
        b.moveToFirst();

        WaitReturnNumB.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            @Override
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(button_D1.this,d1_B.class);  //從待退貨區 到  待退貨區_物流A明細
                startActivity(intent);//開啟Activity
            }
        });

    }
}