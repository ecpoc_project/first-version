package com.example.ivyliang.ec_poc;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ivyliang.ec_poc.data.DBhandler;

import java.util.List;

public class d1_B extends AppCompatActivity {

    private d1_B pthis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_d1_b);   //待退貨區_物流B
        pthis = this;
        Intent intent = this.getIntent();
        initListAdapter();
    }

    private void initListAdapter() {
        ListView listView = (ListView) findViewById(R.id.LVb);
        d1_B.ListAdapter listAdapter = new d1_B.ListAdapter(pthis, R.layout.listview_d1_b, getListRecord());
        listView.setAdapter(listAdapter);
    }

    private List<Listview_d1_b> getListRecord() {
        //搜尋
        DBhandler handler = new DBhandler(this, null, null, 1);
        return handler.getReturnBData();
    }

    class ListAdapter extends ArrayAdapter<Listview_d1_b> {

        private Context context;
        private int resource;
        private LinearLayout item_layout;

        public ListAdapter(@NonNull Context context, int resource, @NonNull List<Listview_d1_b> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                item_layout = new LinearLayout(context);
                LayoutInflater la = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                la.inflate(resource, item_layout, true);
            } else {
                item_layout = (LinearLayout) convertView;
            }
            addItems(getItem(position));

            return item_layout;
        }

        private void addItems(Listview_d1_b item) {
            ((TextView) item_layout.findViewById(R.id.PackageNum)).setText(item.getPackageNum());
            ((TextView) item_layout.findViewById(R.id.CabinetNum)).setText(item.getCabinetNum());
            ((TextView) item_layout.findViewById(R.id.Name)).setText(item.getName());
            ((TextView) item_layout.findViewById(R.id.PhoneNum)).setText(item.getPhoneNum());
            ((TextView) item_layout.findViewById(R.id.Price)).setText(item.getPrice());
            ((TextView) item_layout.findViewById(R.id.AppearanceSize)).setText(item.getAppearanceSize());
            ((TextView) item_layout.findViewById(R.id.AppearanceType)).setText(item.getAppearanceType());
        }
    }
}

