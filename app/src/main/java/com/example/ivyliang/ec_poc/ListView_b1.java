package com.example.ivyliang.ec_poc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListView_b1 extends AppCompatActivity{

    private String CompanyName;
    private String ArrivalDate;
    private String ShouldGoItemA;
    private String ShouldGoItemB;
    public ListView_b1(String companyName, String arrivalDate, String shoulgGoItemA,String shoulgGoItemB) {

        companyName = "今日物流資訊";
        CompanyName = companyName;
        ArrivalDate = arrivalDate;
        ShouldGoItemA = shoulgGoItemA;
        ShouldGoItemB = shoulgGoItemB;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }


    public String getArrivalDate() {
        return ArrivalDate;
    }
    public void setArrivalDate(String arrivalDate) { ArrivalDate = arrivalDate; }

    public String getShouldGoItemA() {
        return ShouldGoItemA;
    }
    public void setShouldGoItemA(String shouldGoItemA) { ShouldGoItemA = shouldGoItemA;}

    public String getShouldGoItemB() {
        return ShouldGoItemB;
    }
    public void setShouldGoItemB(String shouldGoItemB) { ShouldGoItemB = shouldGoItemB;}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_b1);

    }
}
