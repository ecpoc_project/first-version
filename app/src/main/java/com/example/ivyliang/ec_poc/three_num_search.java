package com.example.ivyliang.ec_poc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.ivyliang.ec_poc.data.DBhandler.COL_PhoneNum;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_Price;
import static com.example.ivyliang.ec_poc.data.DBhandler.TABLE_NAME;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_Status;
import com.example.ivyliang.ec_poc.data.DBhandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

import java.util.List;

public class three_num_search extends AppCompatActivity {
    private three_num_search pthis;
    private String num;
    SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three_num_search);  //櫃位內容
        pthis = this;

        Intent intent1 = this.getIntent();
        num = intent1.getStringExtra("ThreePhoneNumber");
        initListAdapter();
    }



    private void initListAdapter() {
        ListView listView = (ListView) findViewById(R.id.lv_threeNum_Result);
        ListAdapter listAdapter = new ListAdapter(pthis, R.layout.listview_three_num_search, getListRecord());
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(three_num_search.this);
                dialog.setTitle("確認取貨");

                dialog.setNegativeButton("NO",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        Toast.makeText(three_num_search.this, "取消",Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.setPositiveButton("YES",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        //開啟資料庫
                        db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);
                        //更新狀態為 -> 已取貨   ->  (手機號碼判斷已取貨)
                        Cursor d = db.rawQuery("UPDATE " + TABLE_NAME + " SET "+ COL_Status + " = " + " '已取貨' " + " WHERE " + COL_PhoneNum + " LIKE " + " '%" + num +"'", null);
                        d.moveToFirst();
                        Toast.makeText(three_num_search.this, "取貨完成",Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.show();
            }
        });
    }

    private List<ListView_ThreeNumberSearch> getListRecord() {
        //搜尋

        DBhandler handler = new DBhandler(this, null, null, 1);
        return handler.getThreeNumSearch(num);
    }

    class ListAdapter extends ArrayAdapter<ListView_ThreeNumberSearch> {

        private Context context;
        private int resource;
        private LinearLayout item_layout;

        public ListAdapter(@NonNull Context context, int resource, @NonNull List<ListView_ThreeNumberSearch> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                item_layout = new LinearLayout(context);
                LayoutInflater l2 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                l2.inflate(resource, item_layout, true);
            } else {
                item_layout = (LinearLayout) convertView;
            }
            addItems(getItem(position));

            return item_layout;
        }

        private void addItems(ListView_ThreeNumberSearch item) {
            ((TextView)item_layout.findViewById(R.id.Name)).setText(item.getName());
            ((TextView)item_layout.findViewById(R.id.Price)).setText(item.getPrice());
            ((TextView)item_layout.findViewById(R.id.AppearanceSize)).setText(item.getAppearanceSize());
            ((TextView)item_layout.findViewById(R.id.AppearanceType)).setText(item.getAppearanceType());
            ((TextView)item_layout.findViewById(R.id.CabinetNum)).setText(item.getCabinetNum());
            ((TextView)item_layout.findViewById(R.id.PhoneNum)).setText(item.getPhoneNum());
        }
    }
}