package com.example.ivyliang.ec_poc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ivyliang.ec_poc.data.DBhandler;

import java.util.List;

public class button_A1_1 extends AppCompatActivity {

    private button_A1_1 pthis;
    private String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__a1_1);  //歷史資料選擇的日期 頁面
        pthis = this;
        //獲取上一个Activity傳過來的值
        Intent intent = this.getIntent();
        //取得傳遞過來的資料
        date = intent.getStringExtra("SelectDate");
        initListAdapter();
        TextView tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setText(date);
    }


    private void initListAdapter() {

        ListView listView = (ListView) findViewById(R.id.lv_HistoryData);

        button_A1_1.ListAdapter listAdapter = new button_A1_1.ListAdapter(pthis, R.layout.listview_a1_1, getListRecord());
        listView.setAdapter(listAdapter);
    }

    private List<ListView_a1_1> getListRecord() {
        //搜尋

        DBhandler handler = new DBhandler(this, null, null, 1);
        return handler.getHistoryInputData(date);
    }

    class ListAdapter extends ArrayAdapter<ListView_a1_1> {

        private Context context;
        private int resource;
        private LinearLayout item_layout;

        public ListAdapter(@NonNull Context context, int resource, @NonNull List<ListView_a1_1> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                item_layout = new LinearLayout(context);
                LayoutInflater li1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                li1.inflate(resource, item_layout, true);
            } else {
                item_layout = (LinearLayout) convertView;
            }
            addItems(getItem(position));

            return item_layout;
        }

        private void addItems(ListView_a1_1 item) {
            ((TextView)item_layout.findViewById(R.id.CompanyName)).setText(item.getCompanyName());
            ((TextView)item_layout.findViewById(R.id.ArrivalDate)).setText(item.getArrivalDate());
            ((TextView)item_layout.findViewById(R.id.ShouldGoItemA)).setText(item.getShouldGoItemA());
            ((TextView)item_layout.findViewById(R.id.ShouldGoItemB)).setText(item.getShouldGoItemB());
            ((TextView)item_layout.findViewById(R.id.AlreadyGoItem)).setText(item.getAlreadyGoItem());
        }

    }
}