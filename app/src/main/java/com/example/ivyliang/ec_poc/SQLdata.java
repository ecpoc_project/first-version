package com.example.ivyliang.ec_poc;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLdata extends SQLiteOpenHelper {

    private final static String DB = "DB2018.db";   //資料庫
    private final static String TB = "TB2018";  //資料表
    private final static int vs = 2;    //版本

    public SQLdata(Context context) {
        // super(context, name, factory, version);
        super(context,DB, null,vs);
        //super(用來打開或創建資料庫 , 資料庫檔案 , 創建游標對象 , 資料庫版本編號)
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //創建資料表語法  : CREATE  TABLE  IF  NOT  EXISTS (會先判斷資料表是否存在，再創建)
        // PRIMARY KEY : 主索引建
        //AUTOINCREMENT : 自動遞增

        String SQL = "CREATE TABLE IF NOT EXISTS "+TB+"(_id INTEGER PRIMARY KEY AUTOINCREMENT , _title VARCHAR(50))";
        sqLiteDatabase.execSQL(SQL);
    }

    // onUpgrade為資料庫版本升級時調用
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // DROP TABLE : 刪除資料表
        String SQL = "DROP TABLE " + TB;
        sqLiteDatabase.execSQL(SQL);
    }
}