package com.example.ivyliang.ec_poc;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.landicorp.android.scan.camera.CameraManager;
import com.landicorp.android.scan.scan.CaptureActivity;
import com.landicorp.android.scan.scan.PreferencesManager;

import java.security.Key;

public class ScanActivity extends CaptureActivity {

    //判斷相機的啟用/關閉
    private boolean isPause;
    private boolean isFlash;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //scan_layout名稱不可更動
        setContentView(R.layout.scan_layout);
        setTheme(R.style.Theme_AppCompat_NoActionBar);
    }

    @Override
    protected void onPause() {
        super.onPause();
        CameraManager c = CameraManager.get();
        if (c != null) {
            // 關閉相機預覽
            c.stopPreview();
            // 釋放相機資源
            CameraManager.closeDriver();
        }
        isPause = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPause) {
            CameraManager c = CameraManager.get();
            if (c != null) {
                //開啟相機預覽
                c.startPreview();
                //重新開啟相機
                CameraManager.openCamera(PreferencesManager.getDefFrontFaceCamera());
            }
            isPause = false;
        }
    }

    /**
     * 客製化的Layout必須在此方法初始化原件並設定元件事件
     **/
    @Override
    public void doInit(Boolean reset) {
        super.doInit(true);
        final ImageButton btn_switch = findViewById(R.id.btn_switch);
        final ImageButton btn_flash_light = findViewById(R.id.btn_flash_light);
        final Button btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        btn_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeSwitch();
            }
        });

        btn_flash_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Camera camera = CameraManager.get().getCamera();
//                Parameters parameters = camera.getParameters();
//                if (!PreferencesManager.getdefFlashLight()) {
//                    PreferencesManager.setdefFlashLight(true);
//                    parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
//                    btn_flash_light.setImageResource(R.drawable.flash_off);
//                } else {
//                    PreferencesManager.setdefFlashLight(false);
//                    parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
//                    btn_flash_light.setImageResource(R.drawable.flash_on);
//                }
//                camera.setParameters(parameters);

                if (isFlashLight(true)) {
                    btn_flash_light.setImageResource(R.drawable.flash_off);
                } else {
                    btn_flash_light.setImageResource(R.drawable.flash_on);
                }

            }
        });

        //前置鏡頭隱藏閃光燈
        if (PreferencesManager.getDefFrontFaceCamera()) {
            btn_flash_light.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                cancel();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 關閉鏡頭，並離開此頁面
     **/
    private void cancel() {
        makeCancel();
        //關閉鏡頭
        CameraManager c = CameraManager.get();
        if (c != null) {
            c.stopPreview(); // 關閉相機預覽
            CameraManager.closeDriver(); // 關閉相機
        }
    }

    public boolean isFlashLight(boolean isEnable) {
        Camera camera = CameraManager.get().getCamera();
        Parameters parameters = camera.getParameters();
        if (isEnable) {
            PreferencesManager.setdefFlashLight(true);
            parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
            return true;
        } else {
            PreferencesManager.setdefFlashLight(false);
            parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            return false;
        }
    }
}
