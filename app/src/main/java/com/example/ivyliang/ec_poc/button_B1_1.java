package com.example.ivyliang.ec_poc;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import static com.example.ivyliang.ec_poc.data.DBhandler.COL_AppearanceSize;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_AppearanceType;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_CabinetNum;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_PackageNum;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_Status;
import static com.example.ivyliang.ec_poc.data.DBhandler.TABLE_NAME;

    public class button_B1_1 extends Activity{

    Button save;
    RadioGroup SizeGroup;
    RadioGroup TypeGroup;
    SQLiteDatabase db;
    String size = ""; //存取Radio的  (大  /  中  /  小)
    String type = ""; //存取Radio的  (箱裝  /  袋裝)
    public String num = "";
    public String text1 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__b1_1);  //選擇櫃位 頁面
        Button save = (Button) findViewById(R.id.save);//取得按鈕物件
        Spinner spinner = (Spinner) findViewById(R.id.CabinetNum); //取得Spinner物件
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.numbers,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Intent intent = this.getIntent();
        //取得傳遞過來的資料
        String name = intent.getStringExtra("PackageNum");
        TextView packageNum = findViewById(R.id.packageNum);
        packageNum.setText(name);

        //兩組Group(分別選大中小 / 箱裝 袋裝)
        SizeGroup = (RadioGroup) findViewById(R.id.size_radioGroup);
        TypeGroup = (RadioGroup) findViewById(R.id.type_radioGroup);

        //開啟資料庫
        db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);
        //name為相機掃到的包裹號 (num設為public負責存取name的值)
         num = name;
        //第一個Group (選大中小)
        SizeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                //判斷選取的Radio 設定要給定的值
                if (i == R.id.size_big) {
                    size = "大";
                } else if (i == R.id.size_middle) {
                    size = "中";
                } else if (i == R.id.size_small) {
                    size = "小";

                }
            }
        });
        //第二個Group (選箱裝 袋裝)
        TypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int j) {
                //判斷選取的Radio 設定要給定的值
                if (j == R.id.type_box) {
                    type = "箱裝";
                }
                else if (j == R.id.type_bag) {
                    type = "袋裝";
                }
            }
        });

        //Spinner 的 Listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view,int position, long id)  {
                String text = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), text,Toast.LENGTH_SHORT).show();
                text1 =text;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //SQL語法(判斷按下Save Button)
    save.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //根據相機掃到的條碼 -> 更新資料庫 COL_AppearanceSize的欄位資料 (商品大小)
               Cursor c = db.rawQuery("UPDATE " + TABLE_NAME + " SET "+ COL_AppearanceSize + " = " +"\""+ size + "\"" + " WHERE " + COL_PackageNum + " = " +"\""+ num+ "\"", null);
               c.moveToFirst();
           //根據相機掃到的條碼 -> 更新資料庫 COL_AppearanceType的欄位資料 (商品型態)
               Cursor d = db.rawQuery("UPDATE " + TABLE_NAME + " SET "+ COL_AppearanceType + " = " +"\""+ type + "\"" + " WHERE " + COL_PackageNum + " = " +"\""+ num+ "\"", null);
               d.moveToFirst();
            //根據相機掃到的條碼 -> 更新資料庫 COL_CabinetNum 的欄位資料 (存放的櫃位)
                Cursor e = db.rawQuery("UPDATE " + TABLE_NAME + " SET "+ COL_CabinetNum + " = " +"\""+ text1 + "\"" + " WHERE " + COL_PackageNum + " = " +"\""+ num+ "\"", null);
                e.moveToFirst();
                //根據選擇櫃位、商品大小 -> 更改商品的狀態為"已上架"
                Cursor f = db.rawQuery("UPDATE " + TABLE_NAME + " SET " + COL_Status + " = " +" '已上架' "+ " WHERE " + COL_PackageNum + " = " +"\""+ num+ "\"",null);
                f.moveToFirst();

            Intent intent = new Intent();   //初始化 Intent   物件
            intent.setClass(button_B1_1.this,button_b1_enter.class);  //從主畫面 到 進貨明細按鈕

            startActivity(intent);//開啟Activity
            finish();
      }
     });
      }
}

