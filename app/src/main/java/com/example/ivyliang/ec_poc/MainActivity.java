package com.example.ivyliang.ec_poc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import static com.example.ivyliang.ec_poc.data.DBhandler.TABLE_NAME;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_ArrivalDate;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_ReturnDate;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_PhoneNum;


import com.example.ivyliang.ec_poc.data.DBhandler;
import java.util.ArrayList;
public class MainActivity extends Activity{
    private View C_btn;
    private MainActivity pthis;
    private static DBhandler dbHandler;
    public EditText three_phone_num;
    public String ThreePhoneNumber;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  //主畫面 頁面

        if(dbHandler == null) {
         dbHandler =  new DBhandler(this, null, null, 1);  //建資料庫
    }

        pthis = this;
        C_btn=(View) findViewById(R.id.C_btn);

        Button A_btn = (Button)findViewById(R.id.A_btn); //取得按鈕物件
        Button B_btn = (Button)findViewById(R.id.B_btn); //取得按鈕物件
        Button D_btn = (Button)findViewById(R.id.D_btn); //取得按鈕物件
        Button search_btn = (Button)findViewById(R.id.search_btn); //取得按鈕物件
        Button btnSetting = (Button)findViewById(R.id.btnSetting); //取得按鈕物件
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(MainActivity.this,import_output_db.class);  //從主畫面 到 進貨明細按鈕

                startActivity(intent);//開啟Activity
            }
        });

        A_btn.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(MainActivity.this,ButtonA1Enter.class);  //從主畫面 到 進貨明細按鈕

                startActivity(intent);//開啟Activity
            }
        });

        B_btn.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(MainActivity.this,button_b1_enter.class);  //從主畫面 到 驗貨上架按鈕

                startActivity(intent);//開啟Activity
            }
        });

        D_btn.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(MainActivity.this,button_D1.class);  //從主畫面 到 退貨明細按鈕

                startActivity(intent);//開啟Activity
            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() //按下按鈕 觸發事件
        {
            @Override
            public  void onClick(View view)
            {

                EditText three_phone_num =(EditText)findViewById(R.id.three_phone_num);
               // three_phone_num.getText().toString();
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(MainActivity.this,three_num_search.class);  //從主畫面 到 驗貨上架按鈕
                startActivity(intent);//開啟Activity
                //變數傳到 DBhandler.java

          // 開啟資料庫
          db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);
          ThreePhoneNumber = three_phone_num.getText().toString().trim();
          Intent intent1 = new Intent();
          intent1.setClass(MainActivity.this,three_num_search.class);
          intent1.putExtra("ThreePhoneNumber", ThreePhoneNumber); //KEY -> ThreePhoneNumber
                startActivity(intent1);
    }
});


    }

    //掃描條碼
    @Override
    protected void onResume() {
        super.onResume();
        C_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(pthis,CameraActivity.class);

                startActivityForResult(intent,0);
                //startActivity(intent)
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle bundle = data.getExtras();
        //txt_response.setText(bundle.getString("scanResult"));
        System.out.print(bundle.getString("Main_result"+"scanResult"));

        Intent intent = new Intent();   //初始化 Intent   物件
        intent.setClass(MainActivity.this,ChangeActivity.class);  //從主畫面 到 驗貨上架按鈕
        intent .putExtra("PackageNum",bundle.getString("scanResult"));
        startActivity(intent);//開啟Activity
        finish();
    }
}