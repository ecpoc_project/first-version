package com.example.ivyliang.ec_poc;

import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class DataBase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "ec"; //資料庫名稱
    public static final String TABLE_NAME = "SummaryTable"; //總資料表
    public static final String COL_CompanyName= "CompanyName";
    public static final String COL_ArrivalDate = "ArrivalDate";
    public static final String COL_ShouldGoItemA = "ShouldGoItemA";
    public static final String COL_ShouldGoItemB = "ShouldGoItemB";
    public static final String COL_PackageNum = "PackageNum";
    public static final String COL_CabinetNum = "CabinetNum";
    public static final String COL_Status = "Status";
    public static final String COL_AppearanceSize= "AppearanceSize";
    public static final String COL_PhoneNum = "PhoneNum";
    public static final String COL_Price = "Price";
    public static final String COL_Name= "Name";
    public static final String COL_ChangeCabinet = "ChangeCabinet";
    public static final String COL_ReturnDate = "ReturnDate";
    public static final String COL_AppearanceType = "AppearanceType";
    public static final String COL_SelectDate = "SelectDate";
    public static final String COL_WaitReturnNum = "WaitReturnNum";
    public static final String COL_LostItem = "LostItem";
    public static final String COL_AlreadyGoItemA = "AlreadyGoItemA";
    public static final String COL_AlreadyGoItemB = "AlreadyGoItemB";


    public static final int DATABASE_VERSION = 1;   //資料庫版本
    private SQLiteDatabase dataBase;

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //建立總表欄位
        //如果不想有空值加上NOT NULL
        //如果想要有預設值就加上DEFAULT
        dataBase = db;
        db.execSQL("CREATE TABLE IF NOT EXISTS SummaryTable (" +
                "CompanyName char(10) NOT NULL," +      //公司名稱(物流A B)
                "ArrivalDate date NOT NULL,"+            //到貨日期
                "ShouldGoItemA char(10) NOT NULL,"+      //應到件數 (物流A)
                "ShouldGoItemB char(10) NOT NULL,"+      //應到件數 (物流B)
                "PackageNum char(20) NOT NULL,"+           //條碼號
                "CabinetNum char(10),"+                    //櫃位號碼
                "Status char(10),"+                        //狀態:
                // 未上架 -> SCAN條碼(X)  CHOOSE櫃位(X)          已理貨 -> SCAN條碼(O)  CHOOSE櫃位(X)
                //已上架 -> SCAN條碼(O)  CHOOSE櫃位(X)               待退貨 -> 等於退貨日期(>=7天)
                "AppearanceSize char(5),"+         //商品外觀
                "PhoneNum char(10) NOT NULL,"+
                "Price Integer NOT NULL,"+
                "Name char(10) NOT NULL,"+
                "ChangeCabinet char(10),"+  //紀錄最新的櫃位
                "ReturnDate Date NOT NULL,"+   //進貨日期+7天
                "AppearanceType char(5),"+      //商品型態
                "SelectDate Date," +
                "WaitReturnNum char(10),"+     //待退貨數
                "LostItem char(10), "+             //差異資料
                "AlreadyGoItemA char(10),"+              //上架件數(物流A)
                "AlreadyGoItemB char(10),"+             //上架件數(物流B)

                "PRIMARY KEY(PackageNum)"+
                ")"
        );


        //退貨A(待退貨區_物流A)
        db.execSQL("SELECT CabinetNum,PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType ,ArrivalDate,ReturnDate,CompanyName FROM SummaryTable WHERE CompanyName = '物流A'  and date('now','localtime') >= ReturnDate ");
        //退貨B(待退貨區_物流B)
        db.execSQL("SELECT CabinetNum,PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType ,ArrivalDate,ReturnDate,CompanyName FROM SummaryTable WHERE CompanyName = '物流B'  and date('now','localtime') >= ReturnDate ");

        //今日進貨資料_物流A
        db.execSQL("SELECT CompanyName, ArrivalDate, ShouldGoItemA FROM SummaryTable WHERE CompanyName = '物流A' and ArrivalDate = date('now')");
        //今日進貨資料_物流B
        db.execSQL("SELECT CompanyName, ArrivalDate, ShouldGoItemB FROM SummaryTable WHERE CompanyName = '物流B' and ArrivalDate = date('now')");

        //歷史資料欄位_物流A
        db.execSQL("SELECT CompanyName,ArrivalDate,ShouldGoItemA,AlreadyGoItemA FROM SummaryTable WHERE CompanyName='物流A' AND SelectDate = ArrivalDate");
        //歷史資料欄位_物流B
        db.execSQL("SELECT CompanyName,ArrivalDate,ShouldGoItemB,AlreadyGoItemB FROM SummaryTable WHERE CompanyName='物流B' AND SelectDate = ArrivalDate");

        //櫃位A01裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A01' and date('now') < ReturnDate");
        //櫃位A02裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A02'  and date('now') < ReturnDate");
        //櫃位A03裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A03'  and date('now') < ReturnDate");
        //櫃位A04裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A04' and date('now') < ReturnDate");
        //櫃位A05裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A05' and date('now') < ReturnDate");
        //櫃位A06裡的欄位
        db.execSQL("SELECT PackageNum,Name,PhoneNum,Price,AppearanceSize,AppearanceType FROM SummaryTable WHERE CabinetNum = 'A06' and date('now') < ReturnDate");

        //手機末3碼搜尋欄位  ->  要存入UI操作所打入的值
        db.execSQL("SELECT CabinetNum, Name, Price, PhoneNum, AppearanceSize, AppearanceType FROM SummaryTable WHERE PhoneNum like '%246'");

    }

    public ArrayList<ListView_b2> getAcceptCabinetData(String tablename){
        Cursor c = dataBase.rawQuery("select * from "+tablename,null);
        ArrayList<ListView_b2> list = new ArrayList<>();
        c.moveToFirst();
        do{
            list.add(new ListView_b2(c.getString(0),c.getString(3), c.getString(1),"", "", c.getString(2)));
        } while(c.moveToNext());

        return list;
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS students");
       // onCreate(db);
    }
}