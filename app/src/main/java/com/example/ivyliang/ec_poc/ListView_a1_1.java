package com.example.ivyliang.ec_poc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListView_a1_1 extends AppCompatActivity {
    private String CompanyName;
    private String ArrivalDate;
    private String ShouldGoItemA;
    private String ShouldGoItemB;
    private String AlreadyGoItem;
    private int a;

    public ListView_a1_1(String companyName, String arrivalDate, String shoulgGoItemA,String shoulgGoItemB,String alreadyGoItem) {
        companyName = "歷史物流資訊";
        CompanyName = companyName;
        ArrivalDate = arrivalDate;
        ShouldGoItemA = shoulgGoItemA;
        ShouldGoItemB = shoulgGoItemB;

        a = (Integer.parseInt(shoulgGoItemA) + Integer.parseInt(shoulgGoItemB));
        alreadyGoItem = Integer.toString(a);

        AlreadyGoItem = alreadyGoItem;
    }

    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }


    public String getArrivalDate() {
        return ArrivalDate;
    }
    public void setArrivalDate(String arrivalDate) { ArrivalDate = arrivalDate; }

    public String getShouldGoItemA() {
        return ShouldGoItemA;
    }
    public void setShouldGoItemA(String shouldGoItemA) { ShouldGoItemA = shouldGoItemA;}

    public String getShouldGoItemB() {
        return ShouldGoItemB;
    }
    public void setShouldGoItemB(String shouldGoItemB) { ShouldGoItemB = shouldGoItemB;}

    public String getAlreadyGoItem() {
        return AlreadyGoItem;
    }
    public void setAlreadyGoItem(String alreadyGoItem) { AlreadyGoItem = alreadyGoItem;}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_a1_1);
    }
}
