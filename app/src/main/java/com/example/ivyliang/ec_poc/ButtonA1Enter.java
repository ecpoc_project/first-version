package com.example.ivyliang.ec_poc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ButtonA1Enter extends AppCompatActivity {
    private Button btTodayData,btHistoryData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_a1_enter);  //進貨資料

        btTodayData = (Button) findViewById(R.id.TodayData);
        btHistoryData = (Button) findViewById(R.id.HistoryData);

        btTodayData.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(ButtonA1Enter.this,button_B1.class);  //從進貨明細按鈕 到 今日進貨資料

                startActivity(intent);//開啟Activity
            }
        });

        btHistoryData.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View arg0)
            {
                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(ButtonA1Enter.this,button_A1.class);  //從進貨明細按鈕 到歷史進貨資料

                startActivity(intent);//開啟Activity
            }
        });

    }
}
