package com.example.ivyliang.ec_poc;

//待退貨區物流A清單

public class Listview_d1_a {

    //宣告變數
    private String PackageNum;
    private String CabinetNum;
    private String Name;
    private String PhoneNum;
    private String Price;
    private String AppearanceSize;
    private String AppearanceType;

    //宣告函數
    public Listview_d1_a(String packageNum, String cabinetNum, String name,String phoneNum, String price, String size, String type) {
        PackageNum = packageNum;
        CabinetNum = cabinetNum;
        Name = name;
        PhoneNum = phoneNum;
        Price = price;
        AppearanceSize = size;
        AppearanceType = type;
    }

    public String getPackageNum() {
        return PackageNum;
    }
    public void setPackageNum(String packageNum) {
        this.PackageNum = packageNum;
    }

    public String getCabinetNum() {
        return CabinetNum;
    }
    public void setCabinetNum(String cabinetNum) { this.CabinetNum = cabinetNum;}

    public String getName() {
        return Name;
    }
    public void setName(String name) {this.Name = name;}

    public String getPhoneNum() {
        return PhoneNum;
    }
    public void setPhoneNum(String phoneNum) {this.PhoneNum= phoneNum;}

    public String getPrice() {
        return Price;
    }
    public void setPrice(String price) {this.Price = price;}

    public String getAppearanceSize() {
        return AppearanceSize;
    }
    public void setAppearanceSize(String size) {this.Price = size;}

    public String getAppearanceType() {
        return AppearanceType;
    }
    public void setAppearanceType(String type) {this.AppearanceType = type;}
}
