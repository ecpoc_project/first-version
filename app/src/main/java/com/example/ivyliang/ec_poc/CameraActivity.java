package com.example.ivyliang.ec_poc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.landicorp.android.scan.camera.CameraManager;
import com.landicorp.android.scan.scan.PreferencesManager;
import com.landicorp.android.scan.scan.ScanDecoder;

/**
 * 使用方式2
 * 1.在需要掃碼的地方調用startActivityForResult此Activity
 * 2.並在onActivityResult取得掃碼結果
 ***/
public class CameraActivity extends AppCompatActivity implements ScanDecoder.ResultCallback {

    private CameraActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        overridePendingTransition(0, 0);
        //初始化相機
        initCamera();
        //前往掃碼頁面
        Intent intent = new Intent(activity, ScanActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    /**
     * 初始化相機
     **/
    private void initCamera() {
        //預設使用前鏡頭  (false)      自拍鏡頭(true)
        PreferencesManager.setDefFrontFaceCamera(false);
        ScanDecoder scanner = new ScanDecoder();
        //預設打開音效
        scanner.setBeepEnable(true);
        if (PreferencesManager.getDefFrontFaceCamera()) {
            scanner.Create(ScanDecoder.CAMERA_ID_FRONT, activity);
        } else {
            scanner.Create(ScanDecoder.CAMERA_ID_BACK, activity);
        }
    }


    /**
     * 取得掃碼結果後回傳
     *
     * @see ScanDecoder.ResultCallback
     **/
    @Override
    public void onResult(String scanResult) {
        Intent intent = new Intent();
        intent.putExtra("scanResult", scanResult);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * 取消
     *
     * @see ScanDecoder.ResultCallback
     **/
    @Override
    public void onCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * 前後鏡頭切換
     *
     * @see ScanDecoder.ResultCallback
     **/
    @Override
    public void onSwitch() {
        Intent intent = new Intent(activity, ScanActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

}

