package com.example.ivyliang.ec_poc;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class import_output_db extends Activity {
    private import_output_db pthis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_output);  //主畫面 頁面
        pthis = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Button Input = findViewById(R.id.Input);
        //將資料庫匯入
        Input.setOnClickListener(new View.OnClickListener() //按下按鈕 觸發事件
        {
            @Override
            public void onClick(View view) {
                try {
                    File sd = Environment.getExternalStorageDirectory();
                    //資料庫的.db檔直接丟SD卡
                    String currentDBPath = Environment.getDataDirectory() + "/data/com.example.ivyliang.ec_poc/databases/ec.db";
                    File currentDB = new File(currentDBPath);
                    File backupDB = new File(sd, "ec.db");
                    System.out.println("SD:"+sd);
                    System.out.println("currentDBPath:"+currentDBPath);
                    if (currentDB.exists()) {
                        FileChannel src = new FileInputStream(backupDB)
                                .getChannel();
                        FileChannel dst = new FileOutputStream(currentDB)
                                .getChannel();
                        dst.transferFrom(src, 0, src.size());
                        src.close();
                        dst.close();
                        Toast.makeText(pthis,"Import成功",Toast.LENGTH_SHORT).show();   //顯示資料庫匯入成功訊息
                    }else{
                        Toast.makeText(pthis,"Import失敗",Toast.LENGTH_SHORT).show();   //顯示資料庫匯入失敗訊息
                    }
                } catch (Exception e) {
                    Toast.makeText(pthis,"Import失敗",Toast.LENGTH_SHORT).show();      //顯示資料庫匯入失敗訊息
                }
            }
        });


        Button Export=findViewById(R.id.Output);
        //將資料庫匯出
        Export.setOnClickListener(new View.OnClickListener() {    //按下按鈕觸發事件
            @Override
            public void onClick(View view) {
                try {
                    File sd = Environment.getExternalStorageDirectory();
//                    String currentDBPath = pthis.getDatabasePath("AlanDB")
//                            .getPath();
                    String currentDBPath = Environment.getDataDirectory() +  "/data/com.example.ivyliang.ec_poc/databases/ec.db";
                    File currentDB = new File(currentDBPath);
                    File backupDB = new File(sd, "ec");

                    System.out.println("SD:" + sd);
                    System.out.println("currentDBPath:" + currentDBPath);

                    FileChannel src = new FileInputStream(currentDB)
                            .getChannel();
                    FileChannel dst = new FileOutputStream(backupDB)
                            .getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(pthis, "Export成功", Toast.LENGTH_SHORT).show();   //顯示資料庫匯出失敗訊息
//                    if (currentDB.exists()) {
//                        FileChannel src = new FileInputStream(currentDB)
//                                .getChannel();
//                        FileChannel dst = new FileOutputStream(backupDB)
//                                .getChannel();
//                        dst.transferFrom(src, 0, src.size());
//                        src.close();
//                        dst.close();
//                        Toast.makeText(pthis, "Export成功", Toast.LENGTH_SHORT).show(); //顯示資料庫匯出成功訊息
//                    }else{
//                        Toast.makeText(pthis, "Export失敗", Toast.LENGTH_SHORT).show(); //顯示資料庫匯出失敗訊息
//                    }   System.out.println(e.toString());
//                    Toast.makeText(pthis, "Export失敗", Toast.LENGTH_SHORT).show();   //顯示資料庫匯出失敗訊息
                } catch(Exception e){
                    System.out.println(e.toString());
                    Toast.makeText(pthis, "Export失敗", Toast.LENGTH_SHORT).show();   //顯示資料庫匯出失敗訊息
                }
            }
        });

        Button btnExit = findViewById(R.id.Exit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(pthis, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
