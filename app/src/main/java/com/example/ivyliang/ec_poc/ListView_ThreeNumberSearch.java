package com.example.ivyliang.ec_poc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

//listview內的變數宣告

public class ListView_ThreeNumberSearch extends AppCompatActivity{
    private String CabinetNum;
    private String Name;
    private String Price;
    private String PhoneNum;
    private String Size;
    private String Type;

    //listview內的變數
    public ListView_ThreeNumberSearch(String cabinetNum, String name, String price,String phoneNum,String size,String type) {
        CabinetNum = cabinetNum;
        Name = name;
        Price = price;
        PhoneNum = phoneNum;
        Size = size;
        Type = type;
    }

    //設定值、取值
    public String getCabinetNum() {
        return CabinetNum;
    }
    public void setCabinetNum(String cabinetNum) {
        CabinetNum = cabinetNum;
    }


    public String getName() {
        return Name;
    }
    public void setName(String name) { Name = name; }

    public String getPrice() {
        return Price;
    }
    public void setPrice(String price) { Price = price;}

    public String getPhoneNum() {return PhoneNum; }
    public void setPhoneNum(String phoneNum) { PhoneNum = phoneNum;}

    public String getAppearanceSize() {
        return Size;
    }
    public void setAppearanceSize(String size) { Size = size;}

    public String getAppearanceType() {
        return Type;
    }
    public void setType(String type) { Type = type;}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_three_num_search);
    }
}
