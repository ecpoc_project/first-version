package com.example.ivyliang.ec_poc;

import java.util.Calendar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.ivyliang.ec_poc.data.DBhandler;

public class button_A1 extends AppCompatActivity {

    private Button btDate,btCheckDate;
    private int mYear,mMonth,mDay;


    /**   Called when the activity is first created.  hello **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button__a1);  //歷史進貨資料頁面

        btDate = (Button) findViewById(R.id.btDate);
        btCheckDate = (Button) findViewById(R.id.btCheckDate);

        btDate.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                showDatePickerDialog();

            }
        });

        btCheckDate.setOnClickListener(new Button.OnClickListener() //按下按鈕 觸發事件
        {
            public  void onClick(View v)
            {
                Intent intent = new Intent();  // 透過Intent 轉跳Activity ,Intent 只是傳遞的媒介
                Bundle bundle = new Bundle();  // Bundle 裡面是裝Activity裡面的資料等於容器

                bundle.putString("SelectDate", btDate.getText().toString());  // 要給他一個key值(名稱)"Message"取得edittext1裡的輸入字串
                intent.putExtras(bundle);    // 放入bundle
                intent.setClass(button_A1.this,button_A1_1.class); // 跳到哪一頁

                //intent.setClass(button_A1.this,DBhandler.class); // 跳到哪一頁
                startActivity(intent);

            }
        });
    }

    public void showDatePickerDialog() {
        // 設定初始日期
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        btDate.setText(year + "-" + (monthOfYear + 1) + "-"
                                + dayOfMonth);
                        //btMissData.setText("今日差異資料");
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

}
