package com.example.ivyliang.ec_poc;

/**
 * Created by Ivy Liang on 2018/10/7.
 */

public class ListView_b2 {     // (選擇) 櫃位的內容
    private String packageNum;
    private String Name;
    private String PhoneNum;
    private String Size;
    private String Type;
    private String Price;

    public ListView_b2(String packageNum, String name, String phoneNum, String size, String type, String price) {
        this.packageNum = packageNum;
        Name = name;
        PhoneNum = phoneNum;
        Size = size;
        Type = type;
        Price = price;
    }

    public String getPackageNum() {
        return packageNum;
    }

    public void setPackageNum(String packageNum) {
        this.packageNum = packageNum;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNum() {
        return PhoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        PhoneNum = phoneNum;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}
