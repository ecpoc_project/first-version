package com.example.ivyliang.ec_poc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Size;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import static com.example.ivyliang.ec_poc.data.DBhandler.COL_AppearanceSize;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_AppearanceType;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_CabinetNum;
import static com.example.ivyliang.ec_poc.data.DBhandler.COL_PackageNum;
import static com.example.ivyliang.ec_poc.data.DBhandler.TABLE_NAME;

public class ChangeActivity extends Activity {
    Button button;
    SQLiteDatabase db;
    //AppearanceSize和 AppearanceType 負責存取目前資料庫已有的(大小 / 型態)
    TextView size;
    TextView type;
    TextView cabinetnum;
    //CabinetNum  負責存取目前資料庫已有的(櫃位號碼)
    public String CabinetNum = "";
    public String Size = "";
    public String Type = "";
    public String num = "";
    public String text1 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changecabinet);  //櫃位調整
        Button button= (Button) findViewById(R.id.button);
        Spinner spinner = (Spinner) findViewById(R.id.ChangeCabinet); //取得Spinner物件
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.number,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Intent intent = this.getIntent();

        //取得傳遞過來的資料
        String name = intent.getStringExtra("PackageNum");
        TextView packageNum = findViewById(R.id.packageNum);
        packageNum.setText(name);

        //name為相機掃到的包裹號 (num設為public負責存取name的值)
        num = name;

        //開啟資料庫
        db = openOrCreateDatabase("ec.db", Context.MODE_PRIVATE, null);

        //Spinner 的 Listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view,int position, long id)  {
                String text = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), text,Toast.LENGTH_SHORT).show();
                text1 =text;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //name為相機掃到的包裹號 (num設為public負責存取name的值)
        Cursor a = db.rawQuery("SELECT *  FROM " + TABLE_NAME +" WHERE " + COL_PackageNum +" = "+ name, null);
        Size = COL_AppearanceSize;
        Type = COL_AppearanceType;
        CabinetNum = COL_CabinetNum;
        size = (TextView)findViewById(R.id.AppearanceSize);
        size.setText("大");

        type = (TextView)findViewById(R.id.AppearanceType);
        type.setText("箱裝");

        cabinetnum = (TextView)findViewById(R.id.CabinetNum);
        cabinetnum.setText("A06");
        a.moveToFirst();




        //判斷按下按鈕
        button.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View view) {  //更換櫃位的SQL語法
                //根據相機掃到的條碼 -> 更新資料庫 COL_CabinetNum 的欄位資料 (存放的櫃位)
                Cursor e = db.rawQuery("UPDATE " + TABLE_NAME + " SET "+ COL_CabinetNum + " = " +"\""+ text1 + "\"" + " WHERE " + COL_PackageNum + " = " +"\""+ num+ "\"", null);
                e.moveToFirst();

                Intent intent = new Intent();   //初始化 Intent   物件
                intent.setClass(ChangeActivity.this,MainActivity.class);  //從主畫面 到 驗貨上架按鈕

                startActivity(intent);//開啟Activity
            }

        });
    }

    private Spinner.OnItemSelectedListener listener = new Spinner.OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
           String sel = arg0.getSelectedItem().toString();
      }

      @Override
       public void onNothingSelected(AdapterView<?> arg0) {

      }
   };
}