package com.example.ivyliang.ec_poc.data;
import android.content.Intent;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import com.example.ivyliang.ec_poc.ListView_ThreeNumberSearch;
import com.example.ivyliang.ec_poc.ListView_a1_1;
import com.example.ivyliang.ec_poc.ListView_b1;
import com.example.ivyliang.ec_poc.ListView_b2;
import com.example.ivyliang.ec_poc.Listview_d1_a;
import com.example.ivyliang.ec_poc.Listview_d1_b;
import com.example.ivyliang.ec_poc.R;
import java.util.ArrayList;
import java.util.List;

public class DBhandler  extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "ec.db"; //資料庫名稱
    public static final String TABLE_NAME = "SummaryTable"; //總資料表
    public static final String COL_CompanyName= "CompanyName";
    public static final String COL_ArrivalDate = "ArrivalDate";
    public static final String COL_ShouldGoItemA = "ShouldGoItemA";
    public static final String COL_ShouldGoItemB = "ShouldGoItemB";
    public static final String COL_PackageNum = "PackageNum";
    public static final String COL_CabinetNum = "CabinetNum";
    public static final String COL_Status = "Status";
    public static final String COL_AppearanceSize= "AppearanceSize";
    public static final String COL_PhoneNum = "PhoneNum";
    public static final String COL_Price = "Price";
    public static final String COL_Name= "Name";
    public static final String COL_ChangeCabinet = "ChangeCabinet";
    public static final String COL_ReturnDate = "ReturnDate";
    public static final String COL_AppearanceType = "AppearanceType";
    public static final String COL_SelectDate = "SelectDate";
    public static final String COL_WaitReturnNum = "WaitReturnNum";
    public static final String COL_LostItem = "LostItem";
    public static final String COL_AlreadyGoItemA = "AlreadyGoItemA";
    public static final String COL_AlreadyGoItemB = "AlreadyGoItemB";
    public static final String COL_WaitReturnNumA ="WaitReturnNumA";
    public static final String COL_WaitReturnNumB ="WaitReturnNumB";
    public static final String COL_ThreePhoneNumber = "ThreePhoneNumber";

    private final Context mContext;

    public SQLiteDatabase dataBase;

    public DBhandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
        mContext = context;
        init();
    }
    //建立資料表
    public void init(){
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_CompanyName + " TEXT NOT NULL," +       //   0
                    COL_ArrivalDate+ " TEXT NOT NULL," +          //   1
                    COL_ShouldGoItemA + " TEXT NOT NULL," +     //     2
                    COL_ShouldGoItemB + " TEXT NOT NULL,"+      //     3
                    COL_PackageNum + " TEXT NOT NULL PRIMARY KEY,"+ //  4
                    COL_CabinetNum + " TEXT,"+       //  5
                    COL_Status + " TEXT,"+           //    6
                    COL_AppearanceSize + " TEXT,"+   //    7
                    COL_PhoneNum + " TEXT NOT NULL,"+    //   8
                    COL_Price + " INTEGER NOT NULL,"+     //   9
                    COL_Name + " TEXT NOT NULL," +     //   10
                    COL_ChangeCabinet +" TEXT," +         //   11
                    COL_ReturnDate+ " TEXT NOT NULL," +    //   12
                    COL_AppearanceType + " TEXT,"+      //   13
                    COL_SelectDate + " TEXT,"+    //   14
                    COL_WaitReturnNum +" TEXT,"+   //   15
                    COL_LostItem + " TEXT,"+      //    16
                    COL_AlreadyGoItemA + " TEXT,"+   //    17
                    COL_AlreadyGoItemB+" TEXT,"+    //   18
                    COL_WaitReturnNumA+" TEXT,"+   //   19
                    COL_WaitReturnNumB + " TEXT)";  //   20
            System.out.println(CREATE_TABLE);
            db.execSQL(CREATE_TABLE);
            Toast.makeText(mContext,"CREATE_TABLE成功",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            //Toast.makeText(mContext,"CREATE_TABLE失敗",Toast.LENGTH_SHORT).show();
        }
    }
@Override
    //OnCreate因為本身Android Studio的內建函式SQLiteOpenHelper，
    //Version(版本) 的問題 ， 進不了onCreate() -> 多一個函式init()
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        //建立總表欄位
        //如果不想有空值加上NOT NULL
        //如果想要有預設值就加上DEFAULT
        try{
            String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_CompanyName + " TEXT NOT NULL," +
                    COL_ArrivalDate+ " TEXT NOT NULL," +
                    COL_ShouldGoItemA + " TEXT NOT NULL," +
                    COL_ShouldGoItemB + " TEXT NOT NULL,"+
                    COL_PackageNum + " TEXT NOT NULL PRIMARY KEY,"+
                    COL_CabinetNum + " TEXT,"+
                    COL_Status + " TEXT,"+
                    COL_AppearanceSize + " TEXT,"+
                    COL_PhoneNum + " TEXT NOT NULL,"+
                    COL_Price + " INTEGER NOT NULL,"+
                    COL_Name + " TEXT NOT NULL," +
                    COL_ChangeCabinet +" TEXT," +
                    COL_ReturnDate +" TEXT NOT NULL,"+
                    COL_AppearanceType + " TEXT,"+
                    COL_SelectDate + " TEXT,"+
                    COL_WaitReturnNum +" TEXT,"+
                    COL_LostItem + " TEXT,"+
                    COL_AlreadyGoItemA + " TEXT,"+
                    COL_AlreadyGoItemB+" TEXT,"+
                    COL_WaitReturnNumA+" TEXT,"+
                    COL_WaitReturnNumB + " TEXT)";

            System.out.println(CREATE_TABLE);
            sqLiteDatabase.execSQL(CREATE_TABLE);
            Toast.makeText(mContext,"CREATE_TABLE成功",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            System.out.println(e.toString());
            //Toast.makeText(mContext,"CREATE_TABLE失敗",Toast.LENGTH_SHORT).show();
        }
    }

    //顯示櫃位內的貨品
    public List<ListView_b2> getAcceptCabinetData(String CabinetNum){
        ArrayList<ListView_b2> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            //判斷櫃位      AND      今日 >= 進貨日期     AND   今日 < 退貨日期
            String sqlCommend = "select * from "+ TABLE_NAME + " WHERE "+  COL_CabinetNum + " = '"+ CabinetNum +"'"+ " AND " + " date('now','localtime') " + " >= " + COL_ArrivalDate+ " AND " + " date('now','localtime') " + " < " + COL_ReturnDate + " AND " + COL_Status + " = " + " '已上架' ";
            Cursor c = db.rawQuery(sqlCommend,null);

            c.moveToFirst();
            do{
                list.add(new ListView_b2(c.getString(4),c.getString(10), c.getString(8),c.getString(7), c.getString(13),"$" + c.getString(9)));
            } while(c.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }

    //今日進貨資料 (物流A B)
    public ArrayList<ListView_b1> getTodayInputData(){
        ArrayList<ListView_b1> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor b=db.rawQuery("SELECT *  FROM  " +TABLE_NAME + " WHERE "+  COL_ArrivalDate + " = "+" date('now','localtime') " + " group by "+ COL_ArrivalDate ,null);
            b.moveToFirst();
            do{
                list.add(new ListView_b1(b.getString(0),b.getString(1), b.getString(2),b.getString(3)));
            } while(b.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }


    //歷史進貨資料 (物流A B)
    public ArrayList<ListView_a1_1> getHistoryInputData(String date){
        ArrayList<ListView_a1_1> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            String sqlCommend2 = "SELECT *  FROM  " +TABLE_NAME + " WHERE "+  COL_ArrivalDate + " = "+ " '"+ date +"' "  + " group by "+ COL_ArrivalDate;
            Cursor p = db.rawQuery(sqlCommend2,null);
            p.moveToFirst();
            do{
                list.add(new ListView_a1_1(p.getString(0),p.getString(1), p.getString(2),p.getString(3),p.getString(17)));
            } while(p.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }

    //搜尋後三碼顯示Listview (確保已進貨 同時尚未到待退貨區的資料)
    public ArrayList<ListView_ThreeNumberSearch> getThreeNumSearch(String ThreePhoneNumber){
      ArrayList<ListView_ThreeNumberSearch> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            String sqlCommend1 = "SELECT *  FROM  " +TABLE_NAME + " WHERE "+ COL_PhoneNum + " like '%" +ThreePhoneNumber + "' " + " and " + " date('now','localtime') " + " >= " + COL_ArrivalDate + " and " + " date('now','localtime') " + " < " + COL_ReturnDate + " and " + COL_Status + " = " + " '已上架' ";
            Cursor i = db.rawQuery(sqlCommend1,null);
            i.moveToFirst();
            do{
                list.add(new ListView_ThreeNumberSearch(i.getString(5),i.getString(10), "$" + i.getString(9),i.getString(8),i.getString(7),i.getString(13)));
            } while(i.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }


    //顯示待退貨區_物流A清單(listview)  -> 顯示出剛好今日要退貨的物品
    public ArrayList<Listview_d1_a> getReturnAData(){
        ArrayList<Listview_d1_a> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor g=db.rawQuery(" SELECT *  FROM " + TABLE_NAME + " WHERE " +  COL_CompanyName + " = " + " '物流A' "  + " and " + COL_ReturnDate + " == "  + " date('now','localtime') " + " and " + COL_Status + " != " + " '已取貨' ",null);
            g.moveToFirst();
            do{
                list.add(new Listview_d1_a(g.getString(4),g.getString(5), g.getString(10),g.getString(8),"$" + g.getString(9),g.getString(7),g.getString(13)));
            } while(g.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }

    //顯示待退貨區_物流B清單(listview)  -> 顯示出剛好今日要退貨的物品
    public ArrayList<Listview_d1_b> getReturnBData(){
        ArrayList<Listview_d1_b> list = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor o=db.rawQuery(" SELECT *  FROM " + TABLE_NAME + " WHERE " +  COL_CompanyName + " = " + " '物流B' "  + " and " + COL_ReturnDate + " == "  + " date('now','localtime') " + " and " + COL_Status + " != " + " '已取貨' ",null);
            o.moveToFirst();
            do{
                list.add(new Listview_d1_b(o.getString(4),o.getString(5), o.getString(10),o.getString(8),"$" + o.getString(9),o.getString(7),o.getString(13)));
            } while(o.moveToNext());
        }catch (Exception e){
            System.out.println(e.toString());
        }
        return list;
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
